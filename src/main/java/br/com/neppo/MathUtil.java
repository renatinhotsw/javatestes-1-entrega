package br.com.neppo;

public class MathUtil {

    public static boolean subsetSumChecker(int ints[], int sum) throws IllegalArgumentException {
        boolean resp = false;
        int aux = 0;
        int soma = 0;



            try{

                for (int i = 0; i < ints.length; i++) {
                    aux = ints[i];

                    for (int j = 1; j < ints.length; j++) {
                        soma += ints[i] + aux;

                        if(soma == sum) {
                            resp = true;
                            break;
                        }else{
                            continue;

                        }

                    }
                }// fim do for
            }catch(NullPointerException e){
                throw new IllegalArgumentException("Conjunto nao pode ser nulo!");
            }

        return resp;
    }// fim método
}// fim da classe